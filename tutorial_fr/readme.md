# 1. Albion : Construction de coupes et volumes à partir de données de sondages

![logo](img/image1.png)

- [1. Albion : Construction de coupes et volumes à partir de données de sondages](#1-albion--construction-de-coupes-et-volumes-à-partir-de-données-de-sondages)
    - [1.1. Introduction](#11-introduction)
    - [1.2. Un peu de vocabulaire](#12-un-peu-de-vocabulaire)
        - [1.2.1. définition d’un nœud `node`](#121-définition-dun-nœud-node)
        - [1.2.2. définition d’un graphe `graph` et d’un segment `edge`](#122-définition-dun-graphe-graph-et-dun-segment-edge)
        - [1.2.3. Notion d’héritage](#123-notion-dhéritage)
        - [1.2.4. Notion de volumes élémentaires](#124-notion-de-volumes-élémentaires)
- [2. Tutoriel](#2-tutoriel)
    - [2.1. Préambule](#21-préambule)
    - [2.2. Importation des données](#22-importation-des-données)
        - [2.2.1. Création d’un projet](#221-création-dun-projet)
        - [2.2.2. Ajout de nouvelles couches](#222-ajout-de-nouvelles-couches)
        - [2.2.3. Importation de fichier individuel](#223-importation-de-fichier-individuel)
        - [2.2.4. Création des sections](#224-création-des-sections)
    - [2.3. Maillage](#23-maillage)
        - [2.3.1. Création d’un maillage](#231-création-dun-maillage)
        - [2.3.2. Nettoyage du maillage](#232-nettoyage-du-maillage)
    - [2.4. Coupes géologiques](#24-coupes-géologiques)
        - [2.4.1. Introduction](#241-introduction)
        - [2.4.2. Création d’un graphe (étape n°1)](#242-création-dun-graphe-étape-n°1)
        - [2.4.3. Création des nœuds (étape n°2 et 3)](#243-création-des-nœuds-étape-n°2-et-3)
        - [2.4.4. Mise à jour des possible edge (étape n°4)](#244-mise-à-jour-des-possible-edge-étape-n°4)
        - [2.4.5. Modification du graphe de manière dynamique (étape n°5)](#245-modification-du-graphe-de-manière-dynamique-étape-n°5)
        - [2.4.6. Création du graphe éditable (étape n°6)](#246-création-du-graphe-éditable-étape-n°6)
        - [2.4.7. Édition de la coupe en vue de modifier le graphe (Etape n°6)](#247-édition-de-la-coupe-en-vue-de-modifier-le-graphe-etape-n°6)
    - [2.5. Construction des coupes minéralisation](#25-construction-des-coupes-minéralisation)
        - [2.5.1. Généralité sur le calcul de la minéralisation](#251-généralité-sur-le-calcul-de-la-minéralisation)
        - [2.5.2. Calcul de la passe minéralisée](#252-calcul-de-la-passe-minéralisée)
        - [2.5.3. Construction des coupes](#253-construction-des-coupes)
        - [2.5.4. Ajout des terminaisons](#254-ajout-des-terminaisons)
    - [2.6. Chargement de données de diagraphie](#26-chargement-de-données-de-diagraphie)
    - [2.7. Création de volume](#27-création-de-volume)
        - [2.7.1. Export du volume](#271-export-du-volume)
        - [2.7.2. Export des sections](#272-export-des-sections)
    - [2.8. Visualiser les logs de sondages](#28-visualiser-les-logs-de-sondages)
        - [2.8.1. Outils QgeoloGIS](#281-outils-qgeologis)
- [Annexes](Albion_aide_annexes.md)

## 1.1. Introduction

Ce tutoriel est accompagné d'une [vidéo de formation](https://prezi.com/view/XAN5W2pWlbirHdQHJsZh/)

La construction volumétrique d’objets géologiques (lithologie, minéralisation…)  est  une tache chronophage, réalisée traditionnellement dans des logiciels miniers de type modeleur. Ces derniers, permettent une représentation 3D, à partir de données de sondages, de coupes. Ces logiciels, souvent complexes sont assez peu ouverts aux données cartographiques et aux outils modernes développés au sein des logiciels de type SIG, rendant les modélisations 3D laborieuses et non reproductibles.

Albion est un logiciel développé dans QGIS, simplifiant la modélisation d’un gisement minier de type stratiforme à partir de coupes multidirectionnelles, en couplant toutes les données cartographiques acquises par le géologue ainsi que les données de sondages. La construction de coupes est grandement facilitée en utilisant la notion de graphe, où chaque donnée géologique observée le long du sondage est interprétée comme le sommet d’un graphe. Cette approche permet de simplifier le travail de digitalisation, elle fournit aux coupes un caractère évolutive, permettant une adaptation rapide des géométries au fur et à mesure de l’acquisition de nouvelles données, ou de nouveaux concepts géologiques et miniers.

La construction volumétrique s’effectue automatiquement par addition de volumes élémentaires. Cette approche innovante permet de créer des volumes complexes, en parfaite adéquation avec les données acquises et les contraintes géologiques. Le  temps de modélisation est ainsi grandement économisé, tout en offrant des volumes parfaitement reproductible et facilement évolutif. Les données d’entrée du logiciel sont au format standard, les données de coupes et de volumes sont facilement exportable vers les logiciels géostatistiques classiques utilisés pour le calcul des ressources et des réserves.

## 1.2. Un peu de vocabulaire

### 1.2.1. définition d’un nœud `node`

La donnée de sondage constitue la donnée d’entrée, elle correspond aussi bien à des données numériques d’enregistrements continus le long de sondage (exemple de données diagraphiques), qu’à des observations ou commentaires géologiques faits lors de la description de carotte ou de cutting. _La nature de ces données et format est présentée au chapitre Format des données de sondages_.

C’est à partir de ces informations observées/enregistrées que sont établis les passes géologiques (lithologie, formation, faciès, minéralisation etc…) qui seront l’élément de base pour la construction de coupes. Une passe (on utilise aussi le terme intersect ou encore génératrice) correspond géométriquement à un segment défini par un point toit et un point mur correspondant à la limite supérieure et inférieure de l’objet géologique intersecté en sondage. Les passes, regroupent des informations géologiques de même nature. **Dans Albion, une passe géologique est un nœud**.

### 1.2.2. définition d’un graphe `graph` et d’un segment `edge`

La corrélation d’une passe de sondage à un autre sondage, le long d’un plan (en général plutôt vertical) constitue une **coupe**, elle permet de contraindre le plus justement possible en plan, la géométrie des objets géologiques que l’on cherche modéliser.

Une coupe où l’ensemble des passes géologiques sont contenues dans un même polygone constitue une **corrélation de sondage de proche en proche**. Dans Albion cette corrélation est matérialisée dans un premier temps par un segment ou `edge` reliant des passes géologique ou `node`. Un graphe est composé d’un ensemble segment ou `edge` joignant les nœuds entres eux.

Les graphes sont non orientés et présentent une topologie de type homogène.

### 1.2.3. Notion d’héritage

Les corps géologiques modélisés ont des propriétés géométriques parfois dépendantes de la géométrie de d’autres corps géologique modélisés :

* la minéralisation d’un gisement à une teneur `A` doit par définition s’emboiter dans le volume minéralisé construit à une teneur supérieure `A*2`
* une minéralisation doit être contenue dans le volume de la formation géologique portant cette minéralisation.

Il est ainsi pertinent pour mieux contraindre le volume modélisé, de tenir compte de cette relation géométrique des objets géologiques entre eux. Ainsi se définit la notion de parent ou d’héritage afin d’inscrire cette dépendance géométrique des corps géologiques entres eux, dans le processus de construction de coupes puis de volumes.

Vue en coupe d'un graphe (ligne bleue-grise) reliant des passes géologiques (bleu foncé). Les passes géologiques correspondent à la formation géologique `B` :

![image](img/image2.png)

La formation géologique `B` est tracée automatiquement à partir du graphe :

![image](img/image3.png)

Cas des passes minéralisées à partir des mesures gamma. La minéralisation est portée par la formation géologique `B`. Le graphe minéralisation est donc hérité du graphe formation `B` :

![image](img/image4.png)

Le graphe de la minéralisation porté par la formation est tracé automatiquement, la minéralisation suit la géométrie de la formation :

![image](img/image5.png)


### 1.2.4. Notion de volumes élémentaires

Au même titre que la passe géologique constitue la brique élémentaire de la construction des coupes, les polygones coupes sont les éléments de base de la construction volumétrique. La modélisation 3D s’effectue par une approche additive, où au droit d’un minimum de trois passes est construit automatiquement un volume « élémentaire », épousant parfaitement la géométrie des polygones coupes en contact avec les passes.

Les volumes élémentaires sont composés d’une surface de type Toit correspondant à une surface triangulée reliant les points Toit des polygones coupes en contact avec les génératrices du volume élémentaire. De même une surface de type Mur construite à partir des points Mur des polygones en contact est créée, ces surfaces Toit/Mur reliées par une surface verticale décrivant le pourtour des deux surfaces Toit/Mur constituent un volume élémentaire qui par addition des autres volumes correspond au volume que l’on cherche à modéliser et ceci quelques soit la complexité du volume construit.

# 2. Tutoriel

## 2.1. Préambule

Afin de faciliter la prise en main du logiciel, des données sont à la disposition des utilisateurs, pour suivre plus facilement ce tutoriel. Elles sont téléchargeables par ce [lien](https://gitlab.com/Oslandia/albion_data/-/archive/master/albion_data-master.zip?path=nt) et le répertoire données (nt.zip) contient :

* N_T_collar.txt : fichier de localisation des têtes de sondages
* N_T_deviation.txt : fichier des données de déviation du sondage
* N_T_formation.txt : fichier de la table formation
* N_T_lithology.txt : fichier de la table lithologie
* NT_avp.txt : fichier de la table radiométrie
* NT_RESI.txt : fichiers de la table diagraphie résistivité

![image](img/image6.png)

## 2.2. Importation des données

### 2.2.1. Création d’un projet

Avant toute construction de coupes, de volumes, il est impératif de créer un projet où seront stockées les données. Cette étape passe par la création d’une base de données PostgreSQL.

![image](img/image7.jpeg)

Action n°1 : Clic gauche sur « New Project (élément de menu) » dans le menu Albion de « QGIS3 »

![image](img/image8.jpeg)

Action n°2 :  Double-Clic gauche sur « Nom (Texte modifiable) » dans « New project name »

![image](img/image9.jpeg)

Action n°3: Double-Clic gauche sur « Nom (Texte modifiable) » dans « New project name »

![image](img/image10.jpeg)

Action n°4: Clic gauche sur « Nom du fichier : » dans « New project name »

![image](img/image11.jpeg)

Action n°5 Saisie au clavier dans « New project name»

![image](img/image12.jpeg)

Action n°6. Clic gauche sur « Oui (bouton poussoir) » dans « Confirmer l’enregistrement »

![image](img/image13.jpeg)

Action n°7 :  Entrer la projection de votre projet. Par défaut le code SRID 32632 est proposé. Clic gauche sur « OK (bouton poussoir) » dans « Project SRID »

![image](img/image14.jpeg)

Action n°8: Dans le cas où le projet existe déjà un message apparait pour confirmer ou non l’ancien projet. Clic gauche sur « Oui (bouton poussoir) » dans « Delete existing DB »

![image](img/image15.jpeg)

Action n°9 : le projet est maintenant créé : il faut charger les données. Clic gauche sur « Albion (élément de menu) » dans « nt - QGIS »

![image](img/image16.jpeg)

Action n°10: Clic gauche sur « Import directory) » dans « menu Albion de QGIS3 »

![image](img/image17.jpeg)

Action n°11: Clic gauche dans « Data directory »

![image](img/image18.jpeg)

Action n°12: Indiquer le répertoire dans laquelle se trouvent les données du tutorial

![image](img/image19.jpeg)

Action n°13. Le logiciel charge l’ensemble des fichiers. La cartographie des sondages apparait à l’écran dans la fenêtre principale de QGIS

![image](img/image20.jpeg)

### 2.2.2. Ajout de nouvelles couches

Les couches présentent dans la fenêtre couches ne sont pas toujours présentent, il est parfois nécessaire d’en ajouter depuis la base de données PostgreSql

Action n° 1: Aller dans le menu QGIS Ajouter une couche Ajouter une couche PostGis

![image](img/image21.jpeg)

Action 2: avec le bouton gauche par l’utilisateur dans « Gestionnaire des sources de données | PostgreSQL »

![image](img/image22.jpeg)

Action n°3: Clic gauche dans « Créer une Nouvelle Connexion PostGIS »

![image](img/image23.jpeg)

Action n°4: compléter comme indiquer dans la figure ci-dessous le  Nom de la connexion , de l’hôte  le port attention il s’agit du port 55432 ! et le nom de la base de données identique au nom de la connexion, vous pouvez ensuite tester la connexion.

![image](img/image24.jpeg)

Action n°5: Clic gauche afin de sauver la connexion, dans le cas où la connexion existe déjà un message apparait vous demander de confirmer la suppression de l’ancienne connexion et la sauvegarde de la nouvelle connexion.

![image](img/image25.jpeg)

Action n°6: Clic gauche dans « Gestionnaire des sources de données | PostgreSQL » et appuyer sur connecter

![image](img/image26.jpeg)

Action n°7: Clic gauche dans « Gestionnaire des sources de données | PostgreSQL » appuyer sur le bouton fermer

![image](img/image26.jpeg)

Action 8: dans le panneau exploreur choisissez la connexion nt que vous venez de créer.

![image](img/image27.jpeg)

Action n° 9 : cliquer deux fois sur la table lithology afin d’ajouter cette table dans le panneau des couches.

![image](img/image28.png)

### 2.2.3. Importation de fichier individuel

Albion permet aussi d’ajouter des couches une par une dans un projet. Cette action se réalise en allant dans le menu Couche de QGIS dans le sous menu Ajouter une couche de texte délimité

![image](img/image30.png)

Dans la fenêtre gestion source de données, charger le fichier texte correspondant aux données que vous souhaitez importer dans le projet Albion.

![image](img/image31.png)

Le fichier devra contenir au moins 4 champs comme indiqué dans la figure ci-dessus. Une nouvelle couche apparait alors dans le panneau couche (A)  Le menu `Import layer` dans Albion (B) permet de créer une couche manipulable avec Albion (C).

![image](img/image32.png)

### 2.2.4. Création des sections

C’est la première étape de construction volumétrique : les sections ou coupes géologique permettront de définir les plans de corrélation de sondages à sondages. Ces plans verticaux ou sub-verticaux de corrélation sont directement guidés par la position des têtes de sondages. Avant de commencer cette étape, précisons qu’en annexe est reportée une ancienne méthode toujours active pour construire les sections.

Mais intéressons-nous dans ce chapitre à une méthode plus récente de construction de coupe. Dans un premier temps nous allons créer les coupes E-W en sélectionnant dans le menu `E-Wx4`.

La création de sections s’effectue en sélectionnant la couche `collar` et en utilisant l’outil sélection de QGIS et en sélectionnant les têtes de sondage appartenant à une même section EW.

Action n°1: Sélectionner `WE x4` dans le menu déroulant de l’outil d’Albion

![image](img/image33.jpeg)

Action n°2 : Utiliser l’outil sélection de QGIS en sélectionnant la couche `collar`

![image](img/image34.jpeg)

Action n°3: Sélectionner les têtes de sondages situés le long d’une ligne EW . Clic gauche pour fermer le polygone de sélection.

![image](img/image35.jpeg)

![image](img/image36.jpeg)

Action n°4  :  Saisie au clavier [... CTRL-ALT-J] afin de construire la première section EW

![image](img/image37.jpeg)

Action n°5 : Recommencer la procédure pour la seconde section EW

![image](img/image38.jpeg)

---

![image](img/image39.png)

* A) B) et C) Une section est créée une fois que les têtes de sondages sont sélectionnés et en appuyant sur les touches Ctrl Alt J.
* D) L’ensemble des coupes E-W sont construites en utilisant la même procédure.
* E) Les coupes N-S sont ensuite crée, en ayant pris soin de bien sélectionner coupe N-S dans le menu combo des outils Albion de QGIS.
* F) L’ensemble de ces coupes NS et EW sont stockées dans la couche `named section` en éditant cette couche vous avez la possibilité d’effacer une section.

A ce stade toutes les sections EW et NS son déjà créées. Nous pouvons désormais faire défiler les sections.

Action n°6 : Rendre la couche `named section` non visible, et faire défiler les coupes EW en utilisant les flèches de l’outil Albion.

![image](img/image40.jpeg)

Le passage d’une coupe EW à l’autre, s’effectue simplement en appuyant sur le bouton `Next Section`. Sous la carte, apparait la vue en coupe de la section avec la trace des sondages tenant compte de la déviation des sondages.

![image](img/image41.jpeg)

En bas à droite de l’écran vous avez la possibilité grâce au paramètres rotation, en indiquant 90° de tourner la vue en carte de 90° afin de mieux visualiser les coupes NS

![image](img/image42.jpeg)

Action n°7: Sélectionner les coupes SN x4, puis Faire défiler les coupes NS utilisant les boutons `next section` ou `previous section`.

![image](img/image43.jpeg)


**Ajoutons maintenant les informations géologiques chargées en début du tutorial.**

Action n°8: Ajoutons la table formation section dans les couches

![image](img/image44.jpeg)

Action n°9: double-clic sur la table `Formation_section`, la couche `formation_section` apparait dans le panneau couche

![image](img/image45.jpeg)

Action n°10: mettre à jour la table `formation_section` en selectionnant la couche et en allant dans le menu Albion `refresh selected layer section`

![image](img/image46.jpeg)

Action n°11 : Effectuons une analyse thématique en fonction du code formation de la couche formation. Double-clic gauche sur la couche [formation-section]

![image](img/image47.jpeg)

Action n°12 : Clic gauche dans « Propriétés de la couche - formation_section | Symbologie »

![image](img/image48.jpeg)

Action n°13: Choisissez l’option « catégorisé »

![image](img/image49.jpeg)

Action n° 14: Choisir la colonne `code` sur laquelle doit s’effectuer l’analyse thématique

![image](img/image50.jpeg)

Action n°15: Clic gauche dans `Propriétés de la couche` > `formation_section` > `Symbologie` > `Classer`

![image](img/image51.jpeg)

Action n°16: Modifier l’épaisseur du trait en sélectionnant tous les éléments et clic gauche

![image](img/image52.jpeg)

Action n°17: Indiquer l’épaisseur du trait Clic gauche dans « Dialogue »

![image](img/image53.jpeg)

Action n°18: refermer la fenêtre en appuyant sur Ok

![image](img/image54.jpeg)

Action n°19: Rotation de la roulette de la souris vers le bas par l’utilisateur dans `*nt - QGIS`

![image](img/image55.jpeg)

Action n°20: Faire défiler les sections en appuyant sur les boutons `next section` ou `previous section` de l’outil Albion.

![image](img/image56.jpeg)

## 2.3. Maillage

### 2.3.1. Création d’un maillage

Le maillage est un élément central de la construction volumétrique d’Albion, Il repose sur la localisation des têtes de sondage aussi il est guidé par la géométrie des sections réalisées précédemment.

Toutes les triangulations des surfaces constituant les volumes modélisés dans Albion, seront directement héritées de ce maillage initial correspondant à une triangulation de Delaunay contrainte par les directions des sections. Le maillage défini les  relations de connexion de sondages à sondages.

Il est créé en allant dans le menu Albion, en sélectionnant `create cell`.

![image](img/image57.jpeg)

Puis dans le panneau couche, en activant la couche cell

![image](img/image58.jpeg)

### 2.3.2. Nettoyage du maillage

Ce maillage réalisé automatiquement nécessite, peut être éventuellement un nettoyage, où quelques liens entres sondages doivent être effacés (voir figure ci-dessous).

Examiner le maillage et contrôler la présence éventuelle de triangles très allongés, en éditant  la couche cell, sélectionnez les triangles à effacer

![image](img/image59.png)

Les triangles de la couche [cell] sont dans un premier temps sélectionnés, la couche [cell] doit être en mode edition, de manière à effacer ces triangles, la couche est ensuite sauvegardée.

![image](img/image60.png)

## 2.4. Coupes géologiques

### 2.4.1. Introduction

Le graphe est l’élément de base des corrélations des passes géologiques dans Albion, il est la colonne vertébrale des coupes et des volumes. Il est constitué de segments `edge` reliant les passes, géologiques, nœud `node`. Dans Albion chaque objet géologique (minéralisation, formation, facies etc…) correspond à un graphe différent. Une minéralisation défini à partir d’un cut of `@100` aura un graphe différent de la minéralisions défini au cut off `@200`.

![image](img/image61.png)

Avant de représenter la minéralisation en coupe, il est nécessaire de représenter en coupe la formation géologique qui porte la minéralisation. Dans le cas de ce tutorial, il s’agit de la formation D présent dans la table formation.

### 2.4.2. Création d’un graphe (étape n°1)

Action n°1 : dans le menu Albion, choisir le menu `New graph`.Dans cette étape nous allons ensemble créer le graphe de la formation D, correspondant dans la table [formation] au code 330.  La formation D, n’est pas une formation géologique contenue à l’intérieure d’une autre formation, ou portée par une autre formation, il s’agit d’une formation sans degré hiérarchique, sans graphe parent.

![image](img/image62.jpeg)

Action n° 2: Saisir au clavier le nom du graphe

![image](img/image63.jpeg)

Action n°3 : Dans cette tache de création d’un graphe formation, celui-ci ne possède pas de graphe parent appuyer sur « OK » dans « Graph »

![image](img/image64.jpeg)

Action n° 4 : Noter la création du graphe dans l’outil Albion

![image](img/image65.jpeg)

### 2.4.3. Création des nœuds (étape n°2 et 3)

Un graphe est composé de nœuds, reste maintenant à créer ceux de la formation D.

Action n°5: Le graphe étant créé, nous allons maintenant placer des nœuds ou `node` dans ce graphe. Sélectionner dans le panneau couche la table formation

![image](img/image66.jpeg)

Action n°6: Clic gauche de la souris et sélectionner l’option `ouvrir table d’attributs`

![image](img/image67.jpeg)

Action n°7: Utiliser le bouton `Filtre` et indiquer le code 330 : puis appuyer sur `sélectionner les entités`

![image](img/image68.jpeg)

Action n°8: la totalité des enregistrements de la table formation contenant le code 300, est maintenant sélectionnée :

![image](img/image69.jpeg)

### 2.4.4. Mise à jour des possible edge (étape n°4)

Action n°9: Vous pouvez fermer la fenêtre formation et retourner dans l’outil Albion pour selectionner le menu `Add Selection to graph node`.

![image](img/image70.jpeg)

Action n°10: Grâce à cette fonctionnalité Albion va copier l’ensemble des enregistrements de la table formation présentant le code 330 à l’intérieur de la table node du graphe Strati_D. Un message de confirmation apparait

![image](img/image71.jpeg)

Action n°11: un graphe reliant les passes geologiques de la table formation avec le code 330 apparait. Ce graphe  ce stade correspond au stade `possible edge`, il est construit automatiquement en fonction des paramètres modifiables stockés dans la table `metadata`.

![image](img/image72.jpeg)

### 2.4.5. Modification du graphe de manière dynamique (étape n°5)

Action n°12 : il est possible de modifier les paramètres de la table `metadata` en l’éditant.

![image](img/image73.jpeg)

Action n°13 : Modifier l’angle de corrélation, de manière à s’assurer de la parfaite corrélation de sondage à sondage. Éditer la table.

![image](img/image74.jpeg)

Action n°14: Ne pas oublier de sauvegarder la table pour que les modifications soient prises en compte.

![image](img/image75.jpeg)

Action n°15 : faire défiler les coupes pour s’assurer que les corrélations de sondages à sondages sont bien tracées. Dans le cas contraire n’hésitez pas à retourner dans la couche `metadata` pour modifier les paramètres de corrélation (Angle et distance).

![image](img/image76.jpeg)

### 2.4.6. Création du graphe éditable (étape n°6)

Action n°16 : Une fois que l’ensemble des corrélations semble correct il faut transférer le graphe `possible_edge` dans `edge`.  Aller dans le menu Albion `Accept graph possible edge`

![image](img/image77.jpeg)

Action n°17: il faut s'assurer que les couches `edge section` et `section polygon` sont bien actives. Apparaitront alors les corrélations sous la forme d’un polygone construit automatiquement. Faire défiler les coupes.

![image](img/image78.jpeg)

Action n°18 : on peut modifier la couleur du polygone en double-cliquant sur la couche `section polygone` ; dans la fenêtre propriété de la couche, choisir le menu `Symbologie`.

![image](img/image79.jpeg)

Action n°19: choisir l’option `catégoriser`

![image](img/image80.jpeg)

Action n°20 : l’analyse thématique se fera sur le champ `graph` de la table `section_polygone`. Appuyer sur `classer` puis sur OK.

![image](img/image81.jpeg)

Action 21: Faire défiler toutes les coupes

![image](img/image82.jpeg)

![image](img/image83.png)

La couche `anchor_section` est composée par défaut de deux enregistrements correspondant aux deux lignes EW et NS visible sur la figure précédente. Ces lignes permettent d’orienter les sections possibles. Par défaut l’échelle verticale des coupes est multipliée par 4. La table étant éditable, tous ces paramètres sont modifiables.

![image](img/image84.png)

### 2.4.7. Édition de la coupe en vue de modifier le graphe (Etape n°6)

Action n°1 : rendre non visible la couche possible_ edge section non visible puisque maintenant nous allons travailler avec la couche edge_section.

![image](img/image85.jpeg)

Action n°2 : rendre éditable la couche edge_section

![image](img/image84.png)

Action n°3: utiliser les outils de sélection de QGIS

![image](img/image85.jpeg)

Action n°4 : selectionner un edge du graphe

![image](img/image86.jpeg)

Action n°5: le trait sélectionné apparaît en couleur jaune. Il est possible de l’effacer en appuyant sur la touche `suppr` de votre clavier ou en utilisant la poubelle de l’outil selection de QGIS.

![image](img/image87.jpeg)

Action n°6: Recommencer l’opération sur un autre edge du graphe. Puis sauver les transformations de la couche `section_edge`

![image](img/image88.jpeg)

Action n°7: Le simple fait de bouger un peu la coupe permet de la _remettre à jour_ et donc de modifier le polygone en fonction du nouveau `edge_section`.

![image](img/image89.jpeg)

Action n°8: le polygone est mis à jour automatiquement en fonction de la couche `edge_section` précédemment modifiée.

![image](img/image90.jpeg)

Action n°9: Pour ajouter un edge dans la table `edge_section`, utiliser le bouton `Ajouter entité linéaire de QGIS`. Tracer ensuite un edge joignant les deux centroïdes des passes géologique de la section où vous souhaitez ajouter un edge.

Clic gauche au départ puis sur le clic droit pour terminer le segment edge.

![image](img/image91.jpeg)

Action n°10 : une fenêtre apparaît, il faut saisir le nom du graphe sur lequel vous travaillez. **Attention** à bien recopier le nom depuis le champ `graph_id` !

![image](img/image92.jpeg)

Action n°11 : Renouveler à nouveau cette tache pour le segment voisin. Clic gauche puis clic droit pour finir le segment

![image](img/image93.jpeg)

Action n°12: À nouveau, renseigner le nom du graphe

![image](img/image94.jpeg)

Action n°13 Sauvegarder la touche et le polygone de la couche `polygone_section` est automatiquement mis à jour en fonction de la couche `edge_section`

![image](img/image95.jpeg)

## 2.5. Construction des coupes minéralisation

### 2.5.1. Généralité sur le calcul de la minéralisation

Une passe minéralisée (génératrice) est définie en fonction des paramètres économiques (cut off, ouverture de chantier et intervalle de dilution). Dans Albion le calcul des passes minéralisées s’effectue à partir des données de radiométrie (champ `eu`) avec les enregistrements de mesures régulières (dans le cas de ce tutorial les données sont dans le fichier `avp`, elles sont renseignées suivant un pas de 10cm).

### 2.5.2. Calcul de la passe minéralisée

La minéralisation telle qu’elle est utilisée pour une estimation, ou la simple compréhension géologique d’un gisement intègre des contraintes géologiques et technico-économiques _via_ la définition de passes minéralisées ou génératrices sur les sondages disponibles.

Dans le cas du logiciel Albion les passes minéralisées sont déterminées par :

1. la coupure sur la radiométrie normalisée, tc (les AVP exprimés en ppm)
2. l’épaisseur minimale d’une passe minéralisée, OC (exprimé en mètres)
3. l’épaisseur minimale d’un intercalaire stérile, IC (exprimé en mètres)

La détermination des limites des génératrices utilise l’algorithme décrit par J.M. Marino (MARINO et al. 1988). Pour chaque sondage les limites sont définies en maximisant par programmation dynamique la valeur récupérée :

Action n°1 : Pour construire les passes minéralisées il faut dans un premier temps créer un graphe comme nous l'avons fait précédemment.

![image](img/image98.jpeg)

Action n°2: Donner au graphe le nom `Min300`, reprenant ainsi la valeur de coupure

![image](img/image99.jpeg)

Action n°3: la minéralisation est portée par la formation géologique `form_D` construite dans l’étape précédente, indiquer dans le champ `parent graphe` le graphe Strati_D

Action n°4: le calcul de la minéralisation s’effectue simplement en allant dans le menu Albion et en choisissant l’option `Compute mineralisation`

![image](img/image100.jpeg)

Action 5 : indiquer les paramètres de cut of puissance minimum et intervalle de stérile.

![image](img/image101.jpeg)

Action n°6: les passes minéralisées sont calculées, les enregistrements sont visibles alors dans la table `mineralisation`

![image](img/image102.jpeg)

Action n°7 : Ouvrir la table `mineralisation`

![image](img/image103.jpeg)

Action n°8: 284 entités ont été crées en fonction des paramètres rentrés précedemment :

- La table `mineralisation` est issue du calcul des passes minéralisées
- `OC` (ouverture de chantier), est la puissance de la passe minéralisée
- `accu` est la teneur moyenne de la passe multiplié par la puissance
-`Grade` correspond à la teneur moyenne de la passe.

![image](img/image104.jpeg)


### 2.5.3. Construction des coupes

Nous ne reprendrons pas ici en détail la construction des graphes, des nœuds, des edge de la couche minéralisation le processus est exactement le même que celui décrit pour le terrain porteur, nous insisterons simplement sur la partie terminaison et superposition des polygones le long des sections.

![image](img/image105.png)

### 2.5.4. Ajout des terminaisons

Les terminaisons des polygones géologiques correspondent dans Albion à des éléments traités de manière indépendante de la construction du graphe. Elles sont construite automatiquement puis éditables avec les outils de QGIS.

![image](img/image106.png)

Il peut arriver que la création d’une terminaison conduise à une superposition de deux polygones appartenant à un même objet géologique. Ces cas de figure ne permettent pas la création d’un modèle volumétrique par addition de volumes élémentaires parfaitement propre d’un point de vue topologique (existence de mur au sein du volume), ainsi Albion signale automatiquement ce genre de problème afin que l’utilisateur corrige manuellement le polygone en déplaçant la terminaison. L’ensemble de zone intersectée est visible dans la couche [current section_intersection].

Exemple de polygones de type minéralisation sans fermeture :

![image](img/image107.png)

Exemple de polygones de type minéralisation avec fermeture :

![image](img/image108.png)

Modification manuelle d’une terminaison :

![image](img/image109.png)

Exemple de superposition de polygone liée à la terminaison d’un polygone. La superposition est symbolisée par un polygone rouge situé dans la couche `current section_intersection` :

![image](img/image110.png)

Il peut arriver que la création d’une terminaison conduise à une superposition de deux polygones appartenant à un même objet géologique. Ces cas de figure ne permettent pas la création d’un modèle volumétrique par addition de volumes élémentaires parfaitement propres d’un point de vue topologique (existence de mur au sein du volume), ainsi Albion signale automatiquement ce genre de problème afin que l’utilisateur corrige manuellement le polygone en déplaçant la terminaison. L’ensemble de zone intersectée est visible dans la couche `current_section_intersection`

## 2.6. Chargement de données de diagraphie

Action n°1: Charger la table `Resistivity_section` depuis la base de données postgreSQL puis mettre à jour  la table à partir du menu Albion `refresh selected layer section`. **Attention** : La mise à jour peut prendre plusieurs minutes !

![image](img/image111.jpeg)

Action n°2: clic gauche dans `Propriétés de la couche` > `resistivity_section` > `Symbologie`

![image](img/image112.jpeg)

Action n°3 : Choisir l’option `Ligne simple` et utiliser le menu assistant

![image](img/image113.jpeg)

Action n°3: choisir le champ dans lequel se trouve la donnée de restitivité dans la table et indiquer les bornes inférieure et supérieure d’affichage.

![image](img/image114.jpeg)

Action n°4 : indiquer le style de jointure `Angle plat` et style de cap : `plat`

![image](img/image115.jpeg)

Action n°5: clic droit sur la souris dans le menu `largeur du trait` ; éditer la formule

![image](img/image116.jpeg)

Action n°6: copier cette formule

![image](img/image117.jpeg)

Action n°7: Fin du glissement de la souris par l’utilisateur dans « *nt - QGIS »

![image](img/image118.jpeg)

Action n°8: clic droit sur le menu `Décalage` ; éditer la formule

![image](img/image119.jpeg)

Action n°9: Coller le presse papier dans `Constructeur de Chaîne d'Expression` et ajouter `x-5` à la fin de l’expression.

![image](img/image120.jpeg)

Action n°10 : Clic gauche sur `Propriétés de la couche` > `resistivity_section` > `Symbologie`

![image](img/image121.jpeg)

Action n°11: visualiser la mesure de résistivité sur l’ensemble des coupes

![image](img/image122.jpeg)

Action n°12: procéder de même pour visualiser la mesure radiométrique en indiquant un décalage de 0.5 dans la chaine d’expression **de manière à décaler la radiométrie de l’autre côté du sondage**.

![image](img/image123.png)


## 2.7. Création de volume

Dans Albion, les volumes sont construits **automatiquement** à partir des coupes multidirectionnelles réalisées pendant l’étape construction de coupe.

Le volume est construit à partir de volume élémentaires additionnels, où au droit de chaque passe géologique, un volume élémentaire parfaitement contraint par la donnée de sondage et des coupes multidirectionnelles est calculé et défini par Albion.

La somme de tous ces volumes élémentaires permet de constituer des volumes complexes à l’image de la représentation 3D des objets géologiques. Enfin soulignons le fait que l’optimisation de la triangulation héritée de la triangulation réalisée lors de l’étape de l’importation des données (voir § Importation de données) assure la parfaite cohérence géométrique du volume créé.

Action n°1 : Le volume à partir de toutes les coupes que nous venons réaliser se construit rapidement en allant dans le menu Albion `Create Volumes`. Cela permettra de construire le volume épousant parfaitement tous les polygones de la table `polygone_section` que nous avons crée dans les étapes précédentes pour le graphe en cours.

![image](img/image124.jpeg)

Action n°2 : Si ce n'est pas le cas, penser à activer le panneau vue 3D de QGIS.

![image](img/image125.jpeg)

Action n°3 : Vous pouvez faire défiler les coupes en vous servant des boutons `next` et `previous` section.

![image](img/image126.jpeg)

Action n°4: Enfin grâce menu déroulant de l’outil 3D Albion, vous pouvez faire afficher le volume  dans la fenêtre 3D

![image](img/image127.png)

Action n°5: Affichage du volume géologique

![image](img/image128.jpeg)

![image](img/image129.png)

### 2.7.1. Export du volume

Le volume construit sous Albion peut être exporté aux formatx `dxf` et `obj`. Ce dernier format de fichier permet d'utiliser les volumes dans le logiciel libre Paraview. L’export des volumes s’effectue en utilisant le menu `volume export`.

![image](img/image130.png)

Exemple d’un export de volume au format `dxf`. Les tests de cohérence géométrique de triangulation indiquent un wireframe de qualité :

![image](img/image131.png)

Exemple de volume crée sous Albion et visualisé avec **Paraview**

![image](img/image132.png)

### 2.7.2. Export des sections

![image](img/image133.png)

![image](img/image134.png)

## 2.8. Visualiser les logs de sondages

### 2.8.1. Outils QgeoloGIS

Il s’agit d’un plugin QGIS développé pour le CEA par Oslandia, qui permet la représentation de logs de sondage à partir des tables chargées dans Albion.

Il faut s’assurer que le plugin est bien chargé en allant dans le menu `extension` de QGIS. Dés que le plugin est chargé l’outils `view Plot log` apparait dans la barre d’outils de QGIS.

![image](img/image135.png)

![image](img/image136.png)

Il est nécessaire de configurer QgeoloGis afin visualiser les sondages à partir de la table collar.

![image](img/image137.png)

Ajoutons maintenant une colonne stratigraphie à partir de la table `formation`. Cette étape se déroule grâce à la fenêtre de QgeoloGis de la figure ci-dessous

![image](img/image138.png)

La table est maintenant importée dans l’outil log, il reste à faire une analyse thématique sur les valeurs importée , après avoir selectionner la colonne Stratigraphie, on utilise le bouton Edit column Style (A) , de manière à réaliser une analyse thématique et catégoriser les différentes valeur de la table formation.

![image](img/image139.png)

![image](img/image140.png)

Enfin on procède de manière identique pour représenter toutes les données de type diagraphie :

![image](img/image141.png)

